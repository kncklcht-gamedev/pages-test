use bevy::{prelude::*, window::PrimaryWindow};

mod bundles;
mod components;
mod styles;
mod systems;

use systems::interactions;
use systems::layout;

use crate::AppState;

use self::components::MainMenuCam;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
  fn build(&self, app: &mut App) {
    app
      .add_systems(OnEnter(AppState::MainMenu), (layout::enter, spawn_camera))
      .add_systems(
        Update,
        (interactions::play, interactions::quit).run_if(in_state(AppState::MainMenu)),
      )
      .add_systems(OnExit(AppState::MainMenu), (layout::exit, despawn_camera));
  }
}

fn spawn_camera(mut commands: Commands, query: Query<&Window, With<PrimaryWindow>>) {
  let window = query.get_single().unwrap();
  let _ = commands
    .spawn((
      Camera2dBundle {
        transform: Transform::from_xyz(window.width() / 2.0, window.height() / 2.0, 1.0),
        ..default()
      },
      MainMenuCam {},
    ))
    .id();
}

fn despawn_camera(mut commands: Commands, query: Query<Entity, With<MainMenuCam>>) {
  if let Ok(camera) = query.get_single() {
    commands.entity(camera).despawn_recursive();
  }
}
