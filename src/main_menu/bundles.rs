use bevy::prelude::*;

use super::styles::*;

pub fn background() -> NodeBundle {
  NodeBundle {
    style: menu_style(),
    background_color: BACKGROUND_COLOR.into(),
    ..default()
  }
}

pub fn title_box() -> NodeBundle {
  NodeBundle {
    style: box_style(),
    background_color: NORMAL_BOX_COLOR.into(),
    ..default()
  }
}

pub fn title_text(asset_server: &Res<AssetServer>) -> TextBundle {
  TextBundle {
    text: Text {
      sections: vec![TextSection::new(
        "Title",
        TextStyle {
          font: asset_server.load(FONT),
          font_size: FONT_SIZE,
          color: NORMAL_TEXT_COLOR,
        },
      )],
      alignment: TextAlignment::Center,
      ..default()
    },
    ..default()
  }
}

pub fn play_button() -> ButtonBundle {
  ButtonBundle {
    style: box_style(),
    background_color: ACCENT_BOX_COLOR.into(),
    ..default()
  }
}

pub fn play_text(asset_server: &Res<AssetServer>) -> TextBundle {
  TextBundle {
    text: Text {
      sections: vec![TextSection::new(
        "Play",
        TextStyle {
          font: asset_server.load(FONT),
          font_size: FONT_SIZE,
          color: ACCENT_TEXT_COLOR,
        },
      )],
      alignment: TextAlignment::Center,
      ..default()
    },
    ..default()
  }
}

pub fn quit_button() -> ButtonBundle {
  ButtonBundle {
    style: box_style(),
    background_color: ALERT_BOX_COLOR.into(),
    ..default()
  }
}

pub fn quit_text(asset_server: &Res<AssetServer>) -> TextBundle {
  TextBundle {
    text: Text {
      sections: vec![TextSection::new(
        "Quit",
        TextStyle {
          font: asset_server.load(FONT),
          font_size: FONT_SIZE,
          color: ALERT_TEXT_COLOR,
        },
      )],
      alignment: TextAlignment::Center,
      ..default()
    },
    ..default()
  }
}
