use bevy::prelude::*;

use crate::{main_menu::components::*, AppState};

pub fn play(
  mut button_query: Query<&Interaction, (Changed<Interaction>, With<PlayButton>)>,
  mut state: ResMut<NextState<AppState>>,
) {
  if let Ok(interaction) = button_query.get_single_mut() {
    match *interaction {
      Interaction::Pressed => state.set(AppState::Game),
      _ => {},
    }
  }
}

pub fn quit(
  mut button_query: Query<&Interaction, (Changed<Interaction>, With<QuitButton>)>,
  mut app_exit_events: ResMut<Events<bevy::app::AppExit>>,
) {
  if let Ok(interaction) = button_query.get_single_mut() {
    match *interaction {
      Interaction::Pressed => app_exit_events.send(bevy::app::AppExit),
      _ => {},
    }
  }
}
