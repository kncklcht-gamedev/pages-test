use bevy::prelude::*;

use crate::main_menu::{
  bundles::*,
  components::{MainMenu, PlayButton, QuitButton},
};

pub fn enter(mut commands: Commands, asset_server: Res<AssetServer>) {
  spawn_main_menu(&mut commands, &asset_server);
}

pub fn exit(mut commands: Commands, query: Query<Entity, With<MainMenu>>) {
  if let Ok(main_menu) = query.get_single() {
    commands.entity(main_menu).despawn_recursive();
  }
}

fn spawn_main_menu(commands: &mut Commands, asset_server: &Res<AssetServer>) {
  commands
    .spawn((background(), MainMenu {}))
    .with_children(|parent| {
      parent.spawn(title_box()).with_children(|parent| {
        parent.spawn(title_text(asset_server));
      });
      parent
        .spawn((play_button(), PlayButton {}))
        .with_children(|parent| {
          parent.spawn(play_text(asset_server));
        });
      parent
        .spawn((quit_button(), QuitButton {}))
        .with_children(|parent| {
          parent.spawn(quit_text(asset_server));
        });
    });
}
