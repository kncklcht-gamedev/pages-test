use bevy::prelude::*;
use bevy::window::{PrimaryWindow, WindowMode};

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
  fn build(&self, app: &mut App) { app.add_systems(Update, full_screen); }
}

fn full_screen(keys: Res<Input<KeyCode>>, mut query: Query<&mut Window, With<PrimaryWindow>>) {
  if keys.just_pressed(KeyCode::F11) {
    if let Ok(mut window) = query.get_single_mut() {
      match window.mode {
        WindowMode::Windowed => window.mode = WindowMode::BorderlessFullscreen,
        WindowMode::BorderlessFullscreen => window.mode = WindowMode::Windowed,
        _ => (),
      }
    }
  }
}
