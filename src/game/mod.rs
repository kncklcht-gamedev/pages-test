use bevy::{prelude::*, window::PrimaryWindow};

use crate::AppState;

mod components;
mod pause_menu;
mod world;

use components::GameCamera;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
  fn build(&self, app: &mut App) {
    app
      .add_plugins((world::Plugin, pause_menu::Plugin))
      .add_systems(OnEnter(AppState::Game), spawn_camera)
      .add_systems(OnExit(AppState::Game), despawn_camera);
  }
}

fn spawn_camera(mut commands: Commands, query: Query<&Window, With<PrimaryWindow>>) {
  let _window = query.get_single().unwrap();
  commands.spawn((
    Camera3dBundle {
      transform: Transform::from_xyz(-2.0, 2.5, 5.0).looking_at(Vec3::ZERO, Vec3::Y),
      ..default()
    },
    UiCameraConfig { show_ui: true },
    GameCamera {},
  ));
}

fn despawn_camera(mut commands: Commands, query: Query<Entity, With<GameCamera>>) {
  if let Ok(camera) = query.get_single() {
    commands.entity(camera).despawn_recursive();
  }
}
