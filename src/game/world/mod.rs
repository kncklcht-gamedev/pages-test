use bevy::prelude::*;

use crate::AppState;

mod components;
mod environment;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
  fn build(&self, app: &mut App) {
    app
      .add_systems(OnEnter(AppState::Game), environment::setup)
      .add_systems(OnExit(AppState::Game), environment::clear);
  }
}
