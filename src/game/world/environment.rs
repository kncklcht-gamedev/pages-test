use bevy::prelude::*;

use super::components::GameObject;

pub fn setup(
  mut commands: Commands,
  mut meshes: ResMut<Assets<Mesh>>,
  mut materials: ResMut<Assets<StandardMaterial>>,
) {
  commands.spawn((
    PbrBundle {
      mesh: meshes.add(shape::Plane::from_size(5.).into()),
      material: materials.add(Color::rgb(0.3, 0.5, 0.3).into()),
      ..default()
    },
    GameObject,
  ));
  commands.spawn((
    PbrBundle {
      mesh: meshes.add(Mesh::from(shape::Cube { size: 1. })),
      material: materials.add(Color::rgb(0.8, 0.7, 0.6).into()),
      transform: Transform::from_xyz(0.0, 0.5, 0.0),
      ..default()
    },
    GameObject,
  ));
  commands.spawn((
    PointLightBundle {
      point_light: PointLight {
        intensity: 1500.,
        shadows_enabled: true,
        ..default()
      },
      transform: Transform::from_xyz(4., 8., 4.),
      ..default()
    },
    GameObject,
  ));
}

pub fn clear(mut commands: Commands, query: Query<Entity, With<GameObject>>) {
  for entity in query.iter() {
    commands.entity(entity).despawn_recursive();
  }
}
