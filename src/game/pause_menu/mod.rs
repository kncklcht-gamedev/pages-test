use bevy::prelude::*;

mod bundles;
mod components;
mod styles;
mod systems;

use systems::layout;

use crate::AppState;

use self::systems::interactions;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
  fn build(&self, app: &mut App) {
    app
      .add_systems(OnEnter(PauseMenuState::Active), (layout::enter,))
      .add_systems(Update, toggle)
      .add_systems(
        Update,
        (interactions::resume, interactions::quit).run_if(in_state(PauseMenuState::Active)),
      )
      .add_systems(OnExit(PauseMenuState::Active), (layout::exit,))
      .add_state::<PauseMenuState>();
  }
}

#[derive(States, Clone, Eq, PartialEq, Hash, Debug, Default)]
pub enum PauseMenuState {
  Active,
  #[default]
  Inactive,
}

fn toggle(
  keys: Res<Input<KeyCode>>,
  app_state: Res<State<AppState>>,
  pause_state: Res<State<PauseMenuState>>,
  mut pause_state_next: ResMut<NextState<PauseMenuState>>,
) {
  if AppState::Game == *app_state.get() {
    if keys.just_pressed(KeyCode::Escape) {
      match pause_state.get() {
        PauseMenuState::Active => {
          pause_state_next.set(PauseMenuState::Inactive);
        },
        PauseMenuState::Inactive => {
          pause_state_next.set(PauseMenuState::Active);
        },
      }
    }
  }
}
