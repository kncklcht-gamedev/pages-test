use bevy::prelude::*;

use crate::{
  game::pause_menu::{
    components::{QuitButton, ResumeButton},
    PauseMenuState,
  },
  AppState,
};

pub fn resume(
  mut button_query: Query<&Interaction, (Changed<Interaction>, With<ResumeButton>)>,
  mut state: ResMut<NextState<PauseMenuState>>,
) {
  if let Ok(interaction) = button_query.get_single_mut() {
    match *interaction {
      Interaction::Pressed => state.set(PauseMenuState::Inactive),
      _ => {},
    }
  }
}

pub fn quit(
  mut button_query: Query<&Interaction, (Changed<Interaction>, With<QuitButton>)>,
  mut app_state: ResMut<NextState<AppState>>,
  mut pause_menu_state: ResMut<NextState<PauseMenuState>>,
) {
  if let Ok(interaction) = button_query.get_single_mut() {
    match *interaction {
      Interaction::Pressed => {
        app_state.set(AppState::MainMenu);
        pause_menu_state.set(PauseMenuState::Inactive);
      },
      _ => {},
    }
  }
}
