use bevy::prelude::*;

use crate::game::pause_menu::{
  bundles::*,
  components::{PauseMenu, QuitButton, ResumeButton},
};

pub fn enter(mut commands: Commands, asset_server: Res<AssetServer>) {
  spawn_menu(&mut commands, &asset_server);
}

pub fn exit(mut commands: Commands, query: Query<Entity, With<PauseMenu>>) {
  if let Ok(pause_menu) = query.get_single() {
    commands.entity(pause_menu).despawn_recursive();
  }
}

fn spawn_menu(commands: &mut Commands, asset_server: &Res<AssetServer>) {
  commands
    .spawn((background(), PauseMenu {}))
    .with_children(|parent| {
      parent.spawn(paused_box()).with_children(|parent| {
        parent.spawn(paused_text(&asset_server));
      });
      parent
        .spawn((resume_button(), ResumeButton {}))
        .with_children(|parent| {
          parent.spawn(resume_text(&asset_server));
        });
      parent
        .spawn((quit_button(), QuitButton {}))
        .with_children(|parent| {
          parent.spawn(quit_text(&asset_server));
        });
    });
}
