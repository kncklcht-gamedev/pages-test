use bevy::{app::PluginGroupBuilder, prelude::*};

mod game;
mod main_menu;
mod window;

fn main() {
  App::new()
    .add_plugins((base_conf(), main_menu::Plugin, game::Plugin, window::Plugin))
    .add_state::<AppState>()
    .run()
}

#[derive(States, Debug, Clone, Copy, Default, Eq, PartialEq, Hash)]
pub enum AppState {
  #[default]
  MainMenu,
  Game,
}

fn base_conf() -> PluginGroupBuilder {
  DefaultPlugins.set(WindowPlugin {
    primary_window: Some(Window {
      title: "Bevy Base Project 3D".to_string(),
      ..default()
    }),
    ..default()
  })
}
